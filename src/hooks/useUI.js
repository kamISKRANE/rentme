import { useContext } from "react";
import MainContext from "../contexts/mainContext";

const useUI = () => {
  const {
    state: { ui: UIState },
    dispatch,
  } = useContext(MainContext);

  const actions = {
    setOnline: (online) => {
      dispatch({
        type: "RECEIVE_ONLINE",
        payload: {
          data: online,
        },
      });
    },
  };

  const selectors = {
    isOnline: () => UIState.online,
  };

  return { selectors, actions };
};

export default useUI;
