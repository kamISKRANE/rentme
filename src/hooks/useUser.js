import { useContext, useEffect } from "react";
import MainContext from "../contexts/mainContext";

import { fetchCart as aFetchCart } from "../contexts/actions/cart";

import {
  fetchOrders as aFetchOrders,
  createOrder as aCreateOrder,
} from "../contexts/actions/orders";

import {
  fetchFavorites as aFetchFavorites,
  setFavorites as aSetFavorites,
} from "../contexts/actions/companies";

import useAuth from "../hooks/useAuth";

const useUser = () => {
  const {
    state: { user: userState },
    dispatch,
  } = useContext(MainContext);

  const { user } = useAuth();

  useEffect(() => {
    actions.fetchFavorites();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (Object.entries(userState.favorites).length) {
      aSetFavorites(userState.favorites);
    }
  }, [userState.favorites]);

  useEffect(() => {
    if (user) {
      actions.fetchOrders(user);
    }
    // eslint-disable-next-line
  }, [user]);

  const actions = {
    addToCart: (item) =>
      dispatch({
        type: "RECEIVE_CART_ITEM",
        payload: {
          company: { id: item.company },
          data: item,
        },
      }),
    setCart: (cart) =>
      dispatch({
        type: "RECEIVE_CART",
        payload: {
          data: cart,
        },
      }),
    fetchCart: () =>
      dispatch({
        type: "RECEIVE_CART",
        payload: {
          data: aFetchCart(),
        },
      }),
    fetchOrders: (user) => {
      aFetchOrders(user).then((data) =>
        dispatch({
          type: "RECEIVE_ORDERS",
          payload: {
            data: data,
          },
        })
      );
    },
    syncOrder: (order) => {
      aCreateOrder(order).then((order) => actions.removeIncomingOrder(order));
    },
    createOrder: (order) => {
      dispatch({
        type: "RECEIVE_INCOMING_ORDER",
        payload: {
          order,
          data: order,
        },
      });
    },
    removeIncomingOrder: (order) => {
      dispatch({
        type: "REMOVE_INCOMING_ORDER",
        payload: {
          order,
        },
      });
    },
    fetchFavorites: () =>
      aFetchFavorites().then((favorites) => {
        dispatch({
          type: "RECEIVE_FAVORITES",
          payload: {
            data: favorites ?? {},
          },
        });
      }),
    setFavorite: (company) => {
      dispatch({
        type: "RECEIVE_FAVORITE",
        payload: {
          company,
        },
      });
    },
    removeFavorite: (company) => {
      dispatch({
        type: "REMOVE_FAVORITE",
        payload: {
          company,
        },
      });
    },
    syncOrders: () => {
      Object.values(selectors.getIncomingOrders()).forEach((order) => {
        order.statut = "waiting";
        actions.syncOrder(order);
      });
    },
  };

  const selectors = {
    getCart: () => userState.cart,
    getCartItems: () => userState.cart.items || [],
    getOrders: () => userState.orders || [],
    getIncomingOrders: () => userState.incomingOrders || [],
    getOrder: (id) => userState.orders[id],
    getOrdersToVote: () =>
      Object.values(userState.orders).find(
        (order) => order.statut.toLowerCase() === "finished"
      ) || null,
    isFavorite: (company) => userState.favorites[company.id] || false,
    getFavorites: () =>
      Object.entries(userState.favorites).filter(
        (isFavorite) => isFavorite[1]
      ) || {},
  };

  return { selectors, actions };
};

export default useUser;
