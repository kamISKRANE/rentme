export const fetchCart = () => JSON.parse(localStorage.getItem("cart")) || [];

export const setCart = (cart) =>
  Promise.resolve().then(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  });
