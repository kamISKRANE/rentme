export const fetchServices = (company) => {
  let services = [
    {
      id: 0,
      company: 1,
      name: "1h de seance en exterieur",
      description:
        "Nous allons faire un peu de sport en exterieur, sur des installations de la ville.",
      options: [
        {
          name: "Pret de matériel",
          type: "radio",
          choices: [
            { name: "Vous apportez votre matériel", price: 0 },
            { name: "Je vous prete mon matériel", price: 3 },
          ],
        },
        {
          name: "Parcours",
          type: "radio",
          choices: [
            { name: "Tour de la ville", price: 0 },
            { name: "Chemins de campagne", price: 0 },
            { name: "En foret", price: 2 },
          ],
        },
      ],
      price: 5.0,
    },
    {
      id: 1,
      company: 1,
      name: "Seance en salle",
      description:
        "Seance de sport en salle de sport partenaire (prix de l'entrée compris)",
      price: 10.0,
    },
    {
      id: 2,
      company: 1,
      name: "Footing sur piste",
      description: "Seance de footing sur une piste olympique",
      price: 5.0,
    },
    {
      id: 3,
      company: 1,
      name: "Footing en ville",
      description:
        "Seance de footing en ville sur un des parcours que j'ai préparé avec soin",
      price: 10.0,
    },
    {
      id: 0,
      company: 0,
      name: "Massage corps",
      description: "Massage de corp par une de nos masseuses professionelles",
      options: [
        {
          name: "Type de massage",
          type: "radio",
          choices: [
            { name: "Doux", price: 0 },
            { name: "Tonique", price: 0 },
            { name: "Aux huiles essentielles bio", price: 10 },
          ],
        },
      ],
      price: 65.0,
    },
    {
      id: 1,
      company: 0,
      name: "Massage visage",
      description:
        "Une de nos masseuses prendra soin de votre visage grâce à nos routines détentes.",
      options: [
        {
          name: "Massage du cuir chevelu",
          type: "radio",
          choices: [
            { name: "Sans", price: 0 },
            { name: "Avec", price: 10 },
          ],
        },
        {
          name: "Soins visage bio à l'aloe vera",
          type: "radio",
          choices: [
            { name: "Sans", price: 0 },
            { name: "Avec", price: 5 },
          ],
        },
      ],
      price: 40.0,
    },
  ];

  return services.filter((val) => val.company === company.id);
};
