import React, { createContext, useEffect, useReducer } from "react";
import { fetchCompanies } from "./actions/companies";
import {
  reducer as CompanyReducer,
  initialState as CompanyInitialState,
} from "./reducers/companies";
import {
  reducer as UserReducer,
  initialState as UserInitialState,
} from "./reducers/user";
import { fetchCart, setCart } from "./actions/cart";
import {
  reducer as UIReducer,
  initialState as UIInitialState,
} from "./reducers/UI";

const MainContext = createContext(null);

const combineReducers = (reducers) => {
  return function rootReducer(state, action) {
    return Object.keys(reducers).reduce((acc, keyReducer) => {
      const newState = { ...acc };
      if (
        !action.stopPropagation &&
        (!action.audience || action.audience === keyReducer)
      ) {
        newState[keyReducer] = reducers[keyReducer](state[keyReducer], action);
      }

      return newState;
    }, state);
  };
};

const rootReducer = combineReducers({
  companies: CompanyReducer,
  user: UserReducer,
  ui: UIReducer,
});

const initialState = {
  companies: CompanyInitialState,
  user: UserInitialState,
  ui: UIInitialState,
};

export const MainProvider = ({ children }) => {
  const [state, dispatch] = useReducer(rootReducer, initialState);

  useEffect(() => {
    dispatch({
      type: "RECEIVE_COMPANIES",
      payload: fetchCompanies(),
    });
    fetchCart() &&
      dispatch({
        type: "RECEIVE_CART",
        payload: {
          data: fetchCart(),
        },
      });
  }, []);

  useEffect(() => {
    if (Object.values(state.user.cart).length) {
      setCart(state.user.cart);
    }
  }, [state.user.cart]);

  return (
    <MainContext.Provider value={{ state, dispatch }}>
      {children}
    </MainContext.Provider>
  );
};

export default MainContext;
