export const initialState = {
  cart: {
    items: [],
  },
  orders: {},
  incomingOrders: {},
  favorites: {},
};

/**
 * action = { type: String, payload: any }
 */
export const reducer = (state, action) => {
  switch (action.type) {
    case "RECEIVE_CART_ITEM":
      return {
        ...state,
        cart: {
          ...state.cart,
          company: action.payload.company.id,
          items: [
            ...(state.cart.items ?? initialState.cart.items),
            action.payload.data,
          ],
          total_price:
            (state.cart.total_price ?? 0) + action.payload.data.total_price,
        },
      };
    case "RECEIVE_CART":
      return {
        ...state,
        cart:
          action.payload.data && Object.values(action.payload.data).length > 0
            ? action.payload.data
            : initialState.cart,
      };
    case "RECEIVE_ORDERS":
      return {
        ...state,
        orders: action.payload.data.reduce((acc, order) => {
          return { ...acc, [order.id]: order };
        }, {}),
      };
    case "RECEIVE_ORDER":
      return {
        ...state,
        orders: {
          ...state.orders,
          [action.payload.data.id]: {
            ...action.payload.data,
            statut:
              action.payload.data === "local"
                ? "waiting"
                : action.payload.data.statut,
          },
        },
      };
    case "RECEIVE_INCOMING_ORDER":
      return {
        ...state,
        incomingOrders: {
          ...state.incomingOrders,
          [action.payload.order.timestamp]: action.payload.data,
        },
      };
    case "REMOVE_INCOMING_ORDER":
      delete state.incomingOrders[action.payload.order.timestamp];
      return state;
    case "RECEIVE_FAVORITE":
      return {
        ...state,
        favorites: {
          ...state.favorites,
          [action.payload.company.id]: true,
        },
      };
    case "RECEIVE_FAVORITES":
      return {
        ...state,
        favorites: action.payload.data ?? {},
      };
    case "REMOVE_FAVORITE":
      return {
        ...state,
        favorites: {
          ...state.favorites,
          [action.payload.company.id]: false,
        },
      };
    default:
      return state;
  }
};
