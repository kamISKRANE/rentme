export const initialState = {
  companies: [],
  services: {},
  votes: {},
};

/**
 * action = { type: String, payload: any }
 */
export const reducer = (state, action) => {
  switch (action.type) {
    case "RECEIVE_COMPANIES":
      return {
        ...state,
        companies: action.payload,
      };
    case "RECEIVE_SERVICES":
      return {
        ...state,
        services: {
          ...state.services,
          [action.payload.company.id]: action.payload.data,
        },
      };
    case "RECEIVE_VOTES":
      return {
        ...state,
        votes: {
          ...state.votes,
          [action.payload.company.id]: action.payload.data,
        },
      };
    default:
      return state;
  }
};
