import React from "react";

import { NavLink } from "react-router-dom";

import useAuth from "../../hooks/useAuth";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faSearch,
  faUser,
  faShoppingBag,
} from "@fortawesome/free-solid-svg-icons";

const Account = () => {
  const { user } = useAuth();

  return (
    <nav>
      <NavLink exact to="/">
        <FontAwesomeIcon icon={faHome} size="lg" /> Accueil
      </NavLink>
      <NavLink to="/search">
        <FontAwesomeIcon icon={faSearch} size="lg" /> Recherche
      </NavLink>
      {user ? (
        <>
          <NavLink to="/orders">
            <FontAwesomeIcon icon={faShoppingBag} size="lg" /> Commandes
          </NavLink>
          <NavLink to="/account">
            <FontAwesomeIcon icon={faUser} size="lg" /> Compte
          </NavLink>
        </>
      ) : (
        <NavLink to="/login">
          <FontAwesomeIcon icon={faUser} size="lg" /> Connexion
        </NavLink>
      )}
    </nav>
  );
};

export default Account;
