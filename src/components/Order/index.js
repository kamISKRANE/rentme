import React from "react";
import { useParams } from "react-router-dom";

import useUser from "../../hooks/useUser";

import OrderVote from "../OrderVote";
import OrderInfos from "../OrderInfos";
import BackHeader from "../BackHeader";

import moment from "moment";

const Order = () => {
  const { id } = useParams();

  const { selectors } = useUser();

  var order = selectors.getOrder(id);

  if (!order) return <></>;

  return (
    <>
      <BackHeader
        title={"Ma commande du " + moment(order.date).format("DD/MM/YYYY")}
      />
      {order.statut === "finished" ? <OrderVote /> : <OrderInfos />}
    </>
  );
};

export default Order;
