import React, { useState } from "react";
import { useParams, useHistory } from "react-router-dom";

import "firebase/database";
import "firebase/firestore";
import firebase from "firebase";

import useUser from "../../hooks/useUser";
import useCompanies from "../../hooks/useCompanies";
import useAuth from "../../hooks/useAuth";

import moment from "moment";

const Order = () => {
  const { id } = useParams();

  const history = useHistory();

  const { selectors } = useUser();
  const { user } = useAuth();
  const {
    selectors: companiesSelectors,
    actions: companiesActions,
  } = useCompanies();

  const [inputs, setInputs] = useState({});

  var order = selectors.getOrder(id);

  if (!order) return <></>;

  const company = companiesSelectors.getCompany(order.company);

  function handleChange(e) {
    setInputs({ ...inputs, [e.target.name]: e.target.value });
  }

  function handleSubmit(e) {
    if (Object.values(inputs).length < 2) {
      return false;
    }

    companiesActions.addVote({
      ...inputs,
      company: company.id,
      user: user.uid,
      date: moment().format("DD/MM/YYYY à HH:MM"),
    });

    firebase
      .firestore()
      .collection("order")
      .doc(order.id)
      .update({ statut: "closed" });

    history.push("/orders");
  }

  return (
    <>
      <div className="page-content page-ordervote">
        <section className="infos content-block">
          Commande n°{order.id} du {order.date} chez {company.name}
        </section>
        <section className="content-block">
          <h2>Qu'avez vous penser de votre commande ?</h2>
        </section>
        <section className="content-block score">
          <h3>Note</h3>
          <label>
            1
            <input
              type="radio"
              value="1"
              onChange={handleChange}
              name="score"
            />
          </label>
          <label>
            2
            <input
              type="radio"
              value="2"
              onChange={handleChange}
              name="score"
            />
          </label>
          <label>
            3
            <input
              type="radio"
              value="3"
              onChange={handleChange}
              name="score"
            />
          </label>
          <label>
            4
            <input
              type="radio"
              value="4"
              onChange={handleChange}
              name="score"
            />
          </label>
          <label>
            5
            <input
              type="radio"
              value="5"
              onChange={handleChange}
              name="score"
            />
          </label>
        </section>
        <section className="content-block">
          <h3>Commentaire </h3>
          <input
            type="text"
            name="comment"
            onChange={handleChange}
            placeholder="Votre avis sur la commande"
          />
        </section>
        <section className="content-block">
          <button onClick={handleSubmit} className="btn btn-valid">
            Enregistrer
          </button>
        </section>
      </div>
    </>
  );
};

export default Order;
