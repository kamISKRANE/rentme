import React, { useEffect } from "react";

import useUI from "../../hooks/useUI";
import useUser from "../../hooks/useUser";

const image = new Image();
let tStart = null;
let tEnd = null;
let counter = 0;
let arrTimes = [];
let abortFallback = false;

const Network = ({ children }) => {
  const { selectors: UISelectors, actions: UIActions } = useUI();
  const { actions: userActions } = useUser();

  function checkConnectivity({
    url = "https://www.google.com/images/phd/px.gif",
    timeToCount = 3,
    threshold = 3000,
    interval = 10000,
  }) {
    reset();
    if (navigator.onLine) {
      changeConnectivity(true);
    } else {
      timeoutFallback(threshold);
    }

    window.addEventListener("online", () => changeConnectivity(true));
    window.addEventListener("offline", () => timeoutFallback(threshold));

    timeoutFallback(threshold);
    checkLatency(url, timeToCount, threshold, (avg) =>
      handleLatency(avg, threshold)
    );
    setInterval(() => {
      reset();
      timeoutFallback(threshold);
      checkLatency(url, timeToCount, threshold, (avg) =>
        handleLatency(avg, threshold)
      );
      userActions.syncOrders();
    }, interval);
  }

  function checkLatency(url, timeToCount, threshold, cb) {
    tStart = Date.now();
    if (counter < timeToCount) {
      image.src = `${url}?t=${tStart}`;
      image.onload = function () {
        abortFallback = true;
        tEnd = Date.now();
        const time = tEnd - tStart;
        arrTimes.push(time);
        counter++;
        checkLatency(url, timeToCount, threshold, cb);
      };
      image.onerror = function () {
        abortFallback = false;
      };
    } else {
      const sum = arrTimes.reduce((a, b) => a + b);
      const avg = sum / arrTimes.length;
      cb(avg);
    }
  }

  function handleLatency(avg, threshold) {
    const isConnectedFast = avg <= threshold;
    changeConnectivity(isConnectedFast);
  }

  function timeoutFallback(threshold) {
    setTimeout(() => {
      if (!abortFallback) {
        changeConnectivity(false);
      }
    }, threshold + 1);
  }

  function reset() {
    arrTimes = [];
    counter = 0;
    abortFallback = false;
  }

  function changeConnectivity(state) {
    const event = new CustomEvent("conectivity-changed", {
      detail: state,
    });
    document.dispatchEvent(event);
    UIActions.setOnline(state);
  }

  useEffect(() => {
    checkConnectivity({});

    document.addEventListener("conectivity-changed", (e) => {
      let online = !!e.detail;
      if (online) {
        console.log("Back online");
      } else {
        console.log("Offline mode");
      }
    });
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (UISelectors.isOnline()) {
      userActions.syncOrders();
      console.log("Connection back, resync orders to firebase.");
    }
    // eslint-disable-next-line
  }, [UISelectors.isOnline()]);

  return <>{children}</>;
};

export default Network;
