import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

import useUser from "../../hooks/useUser";
import useCompanies from "../../hooks/useCompanies";

import firebase from "firebase";

const Order = () => {
  const { id } = useParams();

  const { selectors } = useUser();
  const {
    selectors: companiesSelectors,
    actions: companiesActions,
  } = useCompanies();

  var order = selectors.getOrder(id);

  useEffect(() => {
    if (order) {
      firebase
        .firestore()
        .collection("order")
        .doc(order.id)
        .onSnapshot(function (doc) {
          var tmp_order = doc.data();
          document.querySelector("#badge-statut").classList =
            "badge " + tmp_order.statut;
        });
    }
  }, [order]);

  if (!order) return <></>;

  const company = companiesSelectors.getCompany(order.company);

  if (!companiesSelectors.getServices(company).length)
    companiesActions.fetchServices(company);

  return (
    <div className="page-content page-order">
      <section className="info content-block border-bottom more-hpadding">
        Commande n°{order.id}
      </section>
      <section className="statut content-block border-bottom more-hpadding">
        Statut de la commande
        <br />
        <span id="badge-statut" className={"badge " + order.statut}></span>
      </section>
      <section className="content-block items-recap more-hpadding">
        Recapitulatif de la commande
        <div className="items">
          {order.items &&
            order.items.map((item, index) => {
              const service = companiesSelectors.getService(
                company,
                item.service
              );

              return (
                <div className="item" key={index}>
                  <div className="name">
                    {item.quantity} x {service.name}
                    {item.options &&
                      Object.entries(item.options).map((option) => (
                        <div className="option">
                          {option[0]}: {option[1].name}
                        </div>
                      ))}
                  </div>
                  <div className="price">{item.total_price}</div>
                </div>
              );
            })}
          <div className="flex flex-justify-between additional-price">
            <div className="title">Déplacement</div>
            <div className="price">3</div>
          </div>
          <div className="flex flex-justify-between final-price">
            <div className="title">Total</div>
            <div className="price">{order.total_price}</div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Order;
