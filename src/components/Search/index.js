import React, { useState } from "react";

import useCompanies from "../../hooks/useCompanies";

import CompanyCard from "../CompanyCard";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const Search = () => {
  const { selectors } = useCompanies();

  const [companies, setCompanies] = useState([]);

  function search(e) {
    setCompanies(selectors.searchCompanies(e.target.value));
  }

  return (
    <>
      <div className="page-content page-search">
        <section className="search-bar fixed-top">
          <div className="search">
            <FontAwesomeIcon icon={faSearch} />
            <input
              type="text"
              placeholder="Recherchez un établissement"
              onChange={search}
            ></input>
          </div>
        </section>
        <section className="result">
          {companies &&
            companies.map((company) => (
              <CompanyCard key={company.id} company={company} />
            ))}
        </section>
      </div>
    </>
  );
};

export default Search;
