import React from "react";
import { Link } from "react-router-dom";

import useCompanies from "../../hooks/useCompanies";

import moment from "moment";

const OrderCard = ({ order }) => {
  //const img = require(`../../assets/images/companies/${company.image}.jpg`);

  const { selectors } = useCompanies();

  const company = selectors.getCompany(order.company);

  const switchStatut = function () {
    switch (order.statut) {
      case "finished":
        return (
          <>
            <span>Donnez votre avis</span>
            <span>
              <Link to={"/order/" + order.id} className="btn">
                Voter
              </Link>
            </span>
          </>
        );
      case "closed":
        return <>Commande terminée</>;
      case "local":
        return (
          <span>
            En cours de validation par nos serveurs, nécessite une connexion
            internet.
          </span>
        );
      default:
        return (
          <>
            <span>Commande en cours</span>
            <span>
              <Link to={"/order/" + order.id} className="btn">
                Suivre
              </Link>
            </span>
          </>
        );
    }
  };

  return (
    <div className="order">
      <Link to={"/company/" + company.id}>
        <div
          className="title-company"
          style={{
            backgroundImage:
              "linear-gradient( rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6) ), url('" +
              require(`../../assets/images/companies/${company.image}.jpg`) +
              "')",
          }}
        >
          <span className="name">{company.name}</span>
          <span className="link-to">Afficher la page de l'entreprise</span>
        </div>
      </Link>

      <div className="content">
        <div className="date">
          Commande du {moment(order.date).format("DD/MM/YYYY à HH:mm")}
        </div>
        <div className="statut flex flex-justify-between">{switchStatut()}</div>
        <div className="total-price">
          Prix total: <span className="price">{order.total_price}</span>
        </div>
      </div>

      <Link to={"/order/" + order.id} className="btn btn-flex">
        Afficher le détail
      </Link>
    </div>
  );
};

export default OrderCard;
