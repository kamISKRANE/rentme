import React, { useEffect, useState } from "react";
import useCompanies from "../../hooks/useCompanies";
import useUser from "../../hooks/useUser";

import BackHeader from "../BackHeader";

import { useParams, useHistory } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";

const Service = () => {
  const {
    selectors: companiesSelectors,
    actions: companiesActions,
  } = useCompanies();
  const { actions: usersActions, selectors: usersSelectors } = useUser();

  const [quantity, setQuantity] = useState(1);
  const [options, setOptions] = useState([]);

  const history = useHistory();

  let { companyId, serviceId } = useParams();
  const company = companiesSelectors.getCompany(companyId);
  const service = companiesSelectors.getService(company, serviceId);

  useEffect(() => {
    companiesActions.fetchServices(company);
    // eslint-disable-next-line
  }, [company.id]);

  function addQuantity() {
    setQuantity(quantity + 1);
  }

  function removeQuantity() {
    if (quantity === 1) return;

    setQuantity(quantity - 1);
  }

  function getTotal() {
    return (
      quantity *
      (service.price +
        (options &&
          Object.values(options).reduce(
            (acc, val) => parseFloat(acc + val.price),
            []
          )))
    );
  }
  function addToCart() {
    const cart = usersSelectors.getCart();
    if (cart.company >= 0 && cart.company !== company.id) {
      usersActions.setCart({});
    }

    usersActions.addToCart({
      company: company.id,
      service: service.id,
      options: options,
      quantity: quantity,
      total_price: getTotal(),
    });

    history.push("/company/" + company.id);
  }

  function addOption(option, choice) {
    setOptions({
      ...options,
      [option.name]: choice,
    });
  }

  return (
    <>
      <div
        style={{
          position: "fixed",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          background: "white",
          zIndex: 5,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <BackHeader title={service.name} />
        <div className="page-content page-service">
          <section className="content-block">
            <h3>Description du service</h3>
            {service.description}
          </section>
          {service.options && service.options.length > 0 ? (
            <section className="content-block">
              <h3>Options</h3>
              {service.options.map((option, index) => (
                <div className="option" key={index}>
                  <h4>{option.name}</h4>
                  {option.choices &&
                    option.choices.map((choice, index) => (
                      <label key={index}>
                        <input
                          type={option.type}
                          name={"option-" + index}
                          onChange={(e) => addOption(option, choice)}
                        ></input>
                        {choice.name}{" "}
                        {choice.price > 0 ? (
                          <span className="price fr">+ {choice.price}</span>
                        ) : (
                          ""
                        )}
                      </label>
                    ))}
                </div>
              ))}
            </section>
          ) : null}
          <section className="content-block">
            <h3>Quantitée</h3>
            <div className="flex flex-justify-center flex-align-center quantity">
              <button type="button" onClick={removeQuantity}>
                <FontAwesomeIcon icon={faMinus} />
              </button>
              {quantity}
              <button type="button" onClick={addQuantity}>
                <FontAwesomeIcon icon={faPlus} />
              </button>
            </div>
          </section>
        </div>
        <div className="add-to-cart" style={{ padding: 20 + "px" }}>
          <button type="button" onClick={addToCart} className="btn btn-valid">
            Ajouter {quantity} au panier -{" "}
            <span className="price">{getTotal() || 0}</span>
          </button>
        </div>
      </div>
    </>
  );
};

export default React.memo(Service);
