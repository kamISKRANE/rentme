import React from "react";

const SkeletonImage = () => {
  return <div className="skeleton"></div>;
};

export default SkeletonImage;
