import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Company from "./components/Company";
import CompanyList from "./components/CompanyList";
import { MainProvider } from "./contexts/mainContext";
import Cart from "./components/Cart";
import CartBar from "./components/CartBar";
import Order from "./components/Order";
import Search from "./components/Search";
import OrderList from "./components/OrderList";
import Service from "./components/Service";
import Network from "./components/Network";
import Login from "./components/Login";
import Account from "./components/Account";
import Favorites from "./components/Favorites";
import Register from "./components/Register";
import CompanyComments from "./components/CompanyComments";
import OfflineBar from "./components/OfflineBar";
import Nav from "./components/Nav";

import "./assets/scss/App.scss";

function App() {
  return (
    <MainProvider>
      <Network>
        <Router>
          <OfflineBar />
          <Switch>
            <Route
              exact
              path={[
                "/orders",
                "/orders/:type",
                "/search",
                "/account",
                "/",
                "/login",
                "/register",
              ]}
            >
              <header>
                <CartBar />
                <Nav />
              </header>
              <Switch>
                <Route exact path="/" component={CompanyList} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/search" component={Search} />
                <Route path="/orders" exact component={OrderList} />
                <Route path="/orders/:type" exact component={OrderList} />
                <Route path="/account" component={Account} />
              </Switch>
            </Route>
            <Route path="/company/:id" exact component={Company} />
            <Route
              path="/company/:id/comments"
              exact
              component={CompanyComments}
            />
            <Route
              path="/company/:companyId/service/:serviceId"
              component={Service}
            />
            <Route path="/cart" component={Cart} />
            <Route path="/order/:id" component={Order} />
            <Route path="/favorites" component={Favorites} />
            <Redirect to="/" />
          </Switch>
        </Router>
      </Network>
    </MainProvider>
  );
}

export default App;
